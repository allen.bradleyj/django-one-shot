from django.shortcuts import render, get_object_or_404, redirect
from .models import TodoList, TodoItem
from .forms import TodoListForm, TodoItemForm

def todo_list_list(request):
    list = TodoList.objects.all()
    context = {
        "list_object": list,
    }
    return render(request,"todo/list.html", context)

def todo_list_detail(request, id):
    todo_list_detail = get_object_or_404(TodoList,id=id)
    context = {
        "todo_list_detail": todo_list_detail
    }
    return render(request, "todo/details.html", context)

def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save()
            todo_list_id = todo_list.id
            return redirect(todo_list_detail, todo_list_id)

    else:
        form = TodoListForm

    context = {
        "form": form,
    }

    return render(request, "todo/create.html", context)

def todo_list_update(request,id):
    list = get_object_or_404(TodoList,id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=list)
        if form.is_valid():
            todo_list = form.save()
            todo_list_id = todo_list.id
            return redirect(todo_list_detail, todo_list_id)

    else:
        form = TodoListForm(instance=list)

    context = {
        "todo_list": list,
        "todo_form": form,

    }

    return render(request, "todo/edit.html", context)

def todo_list_delete(request,id):
    list = get_object_or_404(TodoList,id=id)
    if request.method == "POST":
        list.delete()
        return redirect(todo_list_list)

    return render(request, "todo/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todo_item = form.save()
            todo_list_id = todo_item.list.id
            return redirect(todo_list_detail, todo_list_id)

    else:
        form = TodoItemForm

    context = {
        "form": form,
    }
    return render(request, "todo/create.html", context)

def todo_item_update(request,id):
    list = get_object_or_404(TodoItem,id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=list)
        if form.is_valid():
            todo_item = form.save()
            todo_list_id = todo_item.list.id
            return redirect(todo_list_detail, todo_list_id)
    else:
        form = TodoItemForm(instance=list)

    context = {
        "todo_list": list,
        "todo_form": form,

    }
    return render(request, "todo/edit.html", context)
